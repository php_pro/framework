<?php
/**
 * Created by PhpStorm.
 * User: zhangbin
 * Date: 14-3-11
 * Time: 下午4:07
 * 模板类
 */

final Class Template{
    public $template_name = null;
    public $data = array();
    public $out_put = null;

    public function init($template_name,$data = array()){
        $this->template_name = $template_name;
        $this->data = $data;
        $this->fetch();
    }

    /*
     * 加载模板文件
     */
    public function fetch(){
        $view_file = VIEW_PATH. '/' . $this->template_name . '.php';
        if(file_exists($view_file)){
            extract($this->data);   // 从数组中将变量导入到当前的符号表
            ob_start();             // 打开输出控制缓冲
            include $view_file;
            $content = ob_get_contents();
            ob_end_clean();
            $this->out_put = $content;
        }else{
            trigger_error('加载'.$view_file.'模板不存在');
        }
    }
    /*
     * 输出模板
     * return string
     */
    public function outPut(){
        echo $this->out_put;
    }
}