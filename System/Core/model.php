<?php
/**
 * Created by PhpStorm.
 * User: zhangbin
 * Date: 14-3-11
 * Time: 下午3:24
 * 核心模型类
 */
Class Model{
    protected  $db = null;

    final public function __construct(){
        header('Content-type:text/html;charset=utf-8');
        $this-> db = $this->load('mysql');
        $config_db = $this->config('db');
        $this-> db ->init(
            $config_db['db_host'],
            $config_db['db_user'],
            $config_db['db_password'],
            $config_db['db_database'],
            $config_db['db_con'],
            $config_db['db_charset']
        );
    }

    /*
     * 根据表前缀获取表名
     */
    final protected function table($table_name){
        $config_db = $this->config('db');
        return $config_db['db_table_prefix'].$table_name;
    }

    /*
     * 加载类库
     * $lib 类库名称
     * $my  false 默认加载系统自动加载的类库，true 则加载自定义类库
     * return type
     */
    final protected function load($lib,$my = FALSE){
        if(empty($lib)){
            trigger_error('加载类库名不能为空');
        }elseif($my === FAlSE){
            return Application::$_lib[$lib];
        }elseif($my === TRUE){
            return Application::newLib($lib);
        }
    }

    /*
     * 加载系统配置 默认系统配置 $CONFIG['system'][$config]
     * $config 配置名
     */
    final protected function config($config=''){
        return Application::$_config[$config];
    }
}