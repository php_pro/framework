<?php
/**
 * Created by PhpStorm.
 * User: zhangbin
 * Date: 14-3-11
 * Time: 上午10:53
 * 系统配置文件
 */

//数据库配置
//db_con 数据库链接标识  tcon为长久链接  默认即时链接
$CONFIG['system']['db'] = array(
        'db_host' => 'localhost',
        'db_user' => 'root',
        'db_password' => '',
        'db_database' => 'weixin',
        'db_table_prefix' => '',
        'db_charset' => 'utf8',
        'db_con'    => '',
);

//自定义类库配置   自定义类库的文件的前缀
$CONFIG['system']['Lib'] = array(
    'prefix'  => 'Y'
);
//路由 url_type 1、普通模式 index.php?c=controller&a=action&id=2;2、pathinfo模式（待实现）
$CONFIG['system']['route'] = array(
  'default_controller'  =>  'home',
  'default_action'      =>  'index',
  'url_type'            =>  1,
);

//缓存配置   dir 路径相对于根目录   time默认时间1800秒 mode 1为serialize 2保存为可执行文件
$CONFIG['system']['cache'] = array(
    'cache_dir' =>  'cache',
    'cache_prefix'  =>  'cache_',
    'cache_time'    =>  1800,
    'cache_mode'    =>  2,
);