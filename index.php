<?php
/**
 * Created by PhpStorm.
 * User: zhangbin
 * Date: 14-3-11
 * Time: 上午10:48
 * 系统入口文件
 * 引入系统的驱动类   和配置文件，然后运行run()方法
 */

require dirname(__FILE__).'/System/app.php';
require dirname(__FILE__).'/Config/config.php';

Application::run($CONFIG);